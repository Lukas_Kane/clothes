/*
package com.ua.demoClothes.PropertiesHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CORSFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CORSFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Initializing CORSFiler");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest requestToUser = (HttpServletRequest)servletRequest;
        HttpServletResponse responseToUser = (HttpServletResponse)servletResponse;
            responseToUser.setHeader("Access-Control-Allow-Origin", requestToUser.getHeader("Origin"));

            filterChain.doFilter(requestToUser, responseToUser);


    }

    @Override
    public void destroy() {

    }
}
*/
