package com.bunnyclothes.PropertiesHolder;

import com.bunnyclothes.entity.Shoes;
import com.bunnyclothes.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class PropertiesHolder {

    private static SessionFactory factory;
    private static PropertiesHolder propertiesHolder;

    private PropertiesHolder(){createFactory();}

    public static synchronized PropertiesHolder getInstance(){
        if (propertiesHolder == null){propertiesHolder = new PropertiesHolder();}
        return  propertiesHolder;
    }

    public SessionFactory createFactory(){
        try {
            factory =  new Configuration().configure().addPackage("bean")
                    .addAnnotatedClass(Shoes.class)
                    .addAnnotatedClass(User.class)
                    .buildSessionFactory();
        }catch (Throwable ex){
            System.out.println("Error");
            throw new ExceptionInInitializerError(ex);
        }
        return factory;
    }

}
