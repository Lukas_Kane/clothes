package com.bunnyclothes.PropertiesHolder;

import com.bunnyclothes.entity.Shoes;
import com.bunnyclothes.entity.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Shoes.class);
        config.exposeIdsFor(User.class);
        super.configureRepositoryRestConfiguration(config);
    }
}


/*
@Bean
    public Shoes shoes(){
        return new Shoes();
    }


    @Bean
    public ShoesDaoImpl shoesDao(){
        return new ShoesDaoImpl(Shoes.class);
    }
    @Bean
    public ClothesDaoImpl clothesDao(){
        return new ClothesDaoImpl(Clothes.class);
    }

@Bean
    public ShoesController shoesController(){
        return new ShoesController();
    }


    @Bean
    public ShoesService shoesService(){
        return new ShoesService();
    }

    @Bean
    public ClothesService clothesService(){
        return new ClothesService();
    }*/

    /*@Bean
    public ShoesService simpleService(){
        return new ShoesService();
    }*/


