package com.bunnyclothes.controller;

import com.bunnyclothes.api.UserRepository;
import com.bunnyclothes.entity.User;
import com.bunnyclothes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("v1/users")
public class UserController {

    private UserService userService;
    private UserRepository userRepository;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping("/user/test")
    public Page<User> getShoes(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    @GetMapping(path = "user")
    public ResponseEntity<List<User>> getAll(){
        List<User> allUsers = new ArrayList<>();
                userService.findAll().iterator().forEachRemaining(allUsers::add);
        if (allUsers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }
    @GetMapping(path = "user", params = {"id"})
    public ResponseEntity<Optional<User>> getById(
            @RequestParam(value = "id") Integer id){
        Optional<User> user = userService.findOne(id);
        if (user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping(path = "user")
    public ResponseEntity<Void> create(@RequestBody User user){
        //todo create verivication if user already exist in DB
        userService.save(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

  /*  @PutMapping(path = "user", params = {"id"})
    public ResponseEntity<User> update(
            @RequestBody User user, @RequestParam(value = "id") Integer id){
        User currentUser = userService.findOne(id);
        if (currentUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Optional<User> newUser = userService.update(user, id);
        return new ResponseEntity<User>(newUser, HttpStatus.OK);
    }*/
    @DeleteMapping(path = "user", params = {"id"})
    public ResponseEntity<Void> delete(@RequestParam(value = "id") Integer id){
        Optional<User> user = userService.findOne(id);
        if (user == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
