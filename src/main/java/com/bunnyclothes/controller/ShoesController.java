package com.bunnyclothes.controller;

import com.bunnyclothes.api.ShoesRepository;
import com.bunnyclothes.entity.Shoes;
import com.bunnyclothes.service.ShoesService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/items")
public class ShoesController {

    private ShoesService shoesService;
    private ShoesRepository shoesRepository;

    @Autowired
    public ShoesController(ShoesService shoesService, ShoesRepository shoesRepository) {
        this.shoesService = shoesService;
        this.shoesRepository = shoesRepository;
    }

    @GetMapping(path = "/shoes/test", params = {"id"})
    public Optional<Shoes> getShoes(Integer id, Pageable pageable){
        return shoesRepository.findById(id);
    }


    @GetMapping(path = "shoes")
    public ResponseEntity<Iterable<Shoes>> getAll(){
        List<Shoes> allshoes = new ArrayList<>();
                shoesService.findAll().iterator().forEachRemaining(allshoes::add);
        if (allshoes.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(shoesRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping
    public Page<Shoes> getAllShoesByUserId(){
        return null;
    }

    @GetMapping(path = "shoes", params = {"id"})
    public ResponseEntity<Optional<Shoes>> getById(@RequestParam(value = "id") Integer id) {
        Optional<Shoes> shoes = shoesService.findOne(id);
        if (shoes.equals(Optional.empty())){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(shoes, HttpStatus.OK);
    }

    @DeleteMapping(path = "shoes", params = {"id"})
    public ResponseEntity<Void> delete (@RequestParam(value = "id") Integer id){
        Optional<Shoes> shoes = shoesService.findOne(id);
        if (shoes.equals(Optional.empty())) {
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        shoesService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping(path = "shoes", params = {"id"})
    public @ResponseBody ResponseEntity<Shoes> update (@RequestBody Shoes shoes,
                                         @RequestParam("id") Integer shoesFromDb ){
        BeanUtils.copyProperties(shoes, shoesFromDb, "id");
        shoesService.save(shoes);
        return new ResponseEntity<Shoes>(shoes, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(path = "shoes")
    public ResponseEntity<Void> create(@RequestBody Shoes shoes){
        shoesService.save(shoes);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }



/*    @GetMapping("/shoes/season/{id}")
    public String getSeason(@PathVariable("id") Integer id, Optional<Shoes> shoes){
        shoes = shoesService.findOne(1);
        return shoes.getShoesSeason();
    }*/

    @GetMapping("/error")
    public String error(){
        return "/error";
    }

}
