/*
package com.ua.demoClothes.controller;

import com.ua.demoClothes.entity.Clothes;
import com.ua.demoClothes.service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
public class ClothesController {


    @Autowired
    private ClothesService clothesService;

    @GetMapping("/clothes/test")
    public String hello(){
        return "HelloWorld";
    }

    @GetMapping("/clothes/getAll")
    public Iterable<Clothes> findAll(){
        return clothesService.findAll();
    }

    @GetMapping("/clothes/getById/{id}")
    public Clothes getById(@PathVariable("id") Integer id){
        return clothesService.findOne(id);
    }

    @GetMapping("/clothes/save")
    public Clothes save( Clothes clothes){
        //todo remove hard coded values
        clothes.setClothesCode(443);
        clothes.setClothesColor("Whity-white");
        clothes.setClothesSeason("Spring, Autumn");
        clothes.setClothesType("Bosonogki");
        clothes.setClothesPicture("URL");
        return clothesService.save(clothes);
    }

    @GetMapping("/clothes/delete/{id}")
    public Iterable<Clothes> delete (@PathVariable("id") Integer id, Clothes clothes){
        clothes = clothesService.findOne(id);
        clothesService.delete(id);
        return clothesService.findAll();
    }

    @GetMapping("/clothes/season/{id}")
    public String getSeason(@PathVariable("id") Integer id, Clothes clothes){
        clothes = clothesService.findOne(1);
        return clothes.getClothesSeason();
    }


    @GetMapping("/clothes/search/BySeason={season}")
    public List<Clothes> seasonSearch(@PathVariable ("season") String season){
        Iterable<Clothes>clothesList = clothesService.findAll();
        List<Clothes> list = new ArrayList<>();
        for (Clothes clothes :
                clothesList) {
            if (Objects.equals(clothes.getClothesSeason(), season))
                list.add(clothes);
        }
        return list;
    }


    @GetMapping("/error")
    public String error(){
        return "/error";
    }
}
*/
