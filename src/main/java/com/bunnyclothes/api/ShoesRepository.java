package com.bunnyclothes.api;

import com.bunnyclothes.entity.Shoes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository
@Transactional
public interface ShoesRepository extends CrudRepository<Shoes, Serializable> {


}
