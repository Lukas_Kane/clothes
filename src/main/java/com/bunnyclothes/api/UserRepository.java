package com.bunnyclothes.api;

import com.bunnyclothes.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Serializable> {

}
