package com.bunnyclothes.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@javax.persistence.Entity

public class Shoes implements Serializable {

    private Integer id;
    private Integer shoesCode;
    private String shoesColor;
    private String shoesSeason;
    private String shoesType;
    private String shoesPicture;
    private String shoesShortDescription;
    private String shoesFullDescription;
    private String shoesPrice;
    private List<User> userSet;
    public Shoes() {
    }



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShoesCode() {
        return shoesCode;
    }

    public void setShoesCode(Integer shoesCode) {
        this.shoesCode = shoesCode;
    }

    public String getShoesColor() {
        return shoesColor;
    }

    public void setShoesColor(String shoesColor) {
        this.shoesColor = shoesColor;
    }

    public String getShoesSeason() {
        return shoesSeason;
    }

    public void setShoesSeason(String shoesSeason) {
        this.shoesSeason = shoesSeason;
    }

    public String getShoesType() {
        return shoesType;
    }

    public void setShoesType(String shoesType) {
        this.shoesType = shoesType;
    }

    public String getShoesPicture() {
        return shoesPicture;
    }

    public void setShoesPicture(String shoesPicture) {
        this.shoesPicture = shoesPicture;
    }

    public String getShoesShortDescription() {
        return shoesShortDescription;
    }

    public void setShoesShortDescription(String shoesShortDescription) {
        this.shoesShortDescription = shoesShortDescription;
    }

    public String getShoesFullDescription() {
        return shoesFullDescription;
    }

    public void setShoesFullDescription(String shoesFullDescription) {
        this.shoesFullDescription = shoesFullDescription;
    }

    public String getShoesPrice() {
        return shoesPrice;
    }

    public void setShoesPrice(String shoesPrice) {
        this.shoesPrice = shoesPrice;
    }


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_shoes",
    joinColumns = {@JoinColumn(name = "user_id")},
    inverseJoinColumns = {@JoinColumn(name = "shoes_id")})
    public List<User> getUserSet() {
        return this.userSet;
    }

    public void setUserSet(List<User> userSet) {
        this.userSet = userSet;
    }

    @Override
    public String toString() {
        return "Shoes{" +
                "id=" + id +
                ", shoesCode=" + shoesCode +
                ", shoesColor='" + shoesColor + '\'' +
                ", shoesSeason='" + shoesSeason + '\'' +
                ", shoesType='" + shoesType + '\'' +
                ", shoesPicture='" + shoesPicture + '\'' +
                ", shoesShortDescription='" + shoesShortDescription + '\'' +
                ", shoesFullDescription='" + shoesFullDescription + '\'' +
                ", shoesPrice='" + shoesPrice + '\'' +
                '}';
    }
}
