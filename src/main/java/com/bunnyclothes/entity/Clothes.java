/*
package com.ua.demoClothes.entity;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "clothes")
public class Clothes extends Entity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "clothes_id")
    private Integer id;

    @Column(name = "clothes_color")
    private String clothesColor;

    @Column(name = "clothes_code")
    private Integer clothesCode;

    @Column(name = "clothes_season")
    private String clothesSeason;

    @Column(name = "clothes_type")
    private  String clothesType;

    @Column(name = "clothes_picture")
    private String clothesPicture;

    @Column(name = "clothes_short_desc")
    private String clothesShortDesc;
    @Column(name = "clothes_full_desc")
    private String clothesFullDesc;
    @Column(name = "clothes_price")
    private String clothesPrice;


    public Clothes() {
    }

    public Clothes(Integer clothesCode, String clothesColor, String clothesSeason, String clothesType, String clothesPicture) {
        this.clothesColor = clothesColor;
        this.clothesCode = clothesCode;
        this.clothesSeason = clothesSeason;
        this.clothesType = clothesType;
        this.clothesPicture = clothesPicture;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesColor() {
        return clothesColor;
    }

    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor;
    }

    public Integer getClothesCode() {
        return clothesCode;
    }

    public void setClothesCode(Integer clothesCode) {
        this.clothesCode = clothesCode;
    }

    public String getClothesSeason() {
        return clothesSeason;
    }

    public void setClothesSeason(String clothesSeason) {
        this.clothesSeason = clothesSeason;
    }

    public String getClothesType() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }

    public String getClothesPicture() {
        return clothesPicture;
    }

    public void setClothesPicture(String clothesPicture) {
        this.clothesPicture = clothesPicture;
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "id=" + id +
                ", clothesColor='" + clothesColor + '\'' +
                ", clothesCode=" + clothesCode +
                ", clothesSeason='" + clothesSeason + '\'' +
                ", clothesType='" + clothesType + '\'' +
                ", clothesPicture='" + clothesPicture + '\'' +
                ", clothesShortDesc='" + clothesShortDesc + '\'' +
                ", clothesFullDesc='" + clothesFullDesc + '\'' +
                ", clothesPrice='" + clothesPrice + '\'' +
                '}';
    }
}
*/
