/*
package com.ua.demoClothes.service;

import com.ua.demoClothes.api.ClothesRepository;
import com.ua.demoClothes.entity.Clothes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service("clothesService")
@Transactional
public class ClothesService {

    @Autowired
    private ClothesRepository clothesRepository;


    public Iterable<Clothes> findAll() {

        return clothesRepository.findAll();
    }

    public Clothes findOne(Integer id) throws NoSuchElementException {
        return (Clothes) clothesRepository.findOne(id);
    }

    public Clothes save(Clothes clothes){
        return clothesRepository.save(clothes);
    }

    public Integer delete(Integer id){
        clothesRepository.delete(id);
        return id;
    }


}
*/
