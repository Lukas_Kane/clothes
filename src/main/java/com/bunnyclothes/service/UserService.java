package com.bunnyclothes.service;

import com.bunnyclothes.api.UserRepository;
import com.bunnyclothes.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("userService")
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> findAll(){
        return (List<User>) userRepository.findAll();
    }
    public Optional<User> findOne(Integer id){
        return userRepository.findById(id);
    }
    public User save (User user){
        return userRepository.save(user);
    }
  /*  public Optional<User> update(User newUser, Integer id){
        Optional<User> user = userRepository.findById(id);
        user.ifPresent(newUser.getUserLogin());
        user.setUserPassword(newUser.getUserPassword());
        user.setUserFirstName(newUser.getUserFirstName());
        user.setUserLastName(newUser.getUserLastName());
        user.setUserEmail(newUser.getUserEmail());
        user.setUserBirthDay(newUser.getUserBirthDay());
        user.setUserFacebookId(newUser.getUserFacebookId());
        user.setUserGoogleId(newUser.getUserGoogleId());
        user.setUserAvatar(newUser.getUserAvatar());
        return userRepository.save(user);
    }*/
    public void delete (Integer id){
        userRepository.deleteById(id);
    }

}
