package com.bunnyclothes.service;

import com.bunnyclothes.api.ShoesRepository;
import com.bunnyclothes.api.UserRepository;
import com.bunnyclothes.entity.Shoes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("shoesService")
public class ShoesService {


    @Autowired
    private ShoesRepository shoesMainRepository;
    @Autowired
    private UserRepository userRepository;



    public List<Shoes> findAll() {

        return (List<Shoes>) shoesMainRepository.findAll();
    }

    public Optional<Shoes> findById(Integer id){
        return shoesMainRepository.findById(id);
    }

    public Optional<Shoes> findOne(Integer id){
        return  shoesMainRepository.findById(id);
    }

    public Shoes save(Shoes shoes){
        return shoesMainRepository.save(shoes);
    }


    public Shoes update(Shoes newShoes, Integer id){
        return shoesMainRepository.save(newShoes);
    }
    public void delete(Integer id){
        shoesMainRepository.deleteById(id);
    }

}
