<%--
  Created by IntelliJ IDEA.
  User: andre
  Date: 03.10.2017
  Time: 16:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Shoes</title>
</head>
<body>
<form userSet="shoesForm" name="shoesForm" method="post" action="${pageContext.servletContext.contextPath}/addShoes">
    <h3>Shoes Code</h3>
    <input class="inputField" type="text" name="shoesCode" value="${shoes.shoesCode}">
    <h3>Shoes Color</h3>
    <input class="inputField" type="text" name="shoesColor" value="${shoes.shoesColor}">
    <h3>Shoes Season</h3>
    <input class="inputField" type="text" name="shoesSeason" value="${shoes.shoesSeason}"/>
    <h3>Shoes Type</h3>
    <input class="inputField" type="text" name="shoesType" value="${shoes.shoesType}"/>
    <input class="button" type="submit" value="Add Shoes"/>
    <a href="${pageContext.servletContext.contextPath}/allShoes"><input type="button" value="">All</a>
</form>
</body>
</html>
